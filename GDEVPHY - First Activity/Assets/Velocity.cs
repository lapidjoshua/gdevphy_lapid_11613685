﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Velocity : MonoBehaviour {

    public float angle = 0;
    public float speed = 1;
    public Vector velocity;
    public float xPosition;
    public float yPosition;
    float smooth = 5.0f;
    float tiltAngle = 60.0f;

    // Use this for initialization
    void Start () {
        velocity = new Vector(xPosition, yPosition);
	}
	
	// Update is called once per frame
	void Update () {
        xPosition = Input.GetAxis("Horizontal");
        yPosition = Input.GetAxis("Vertical");
        transform.position += (new Vector3(xPosition, yPosition, 0) * speed) * Time.deltaTime;
        xPosition += velocity.x;
        yPosition += velocity.y;
        
        var angle = Mathf.Atan2(yPosition, xPosition) * Mathf.Rad2Deg;
        if (Input.anyKey)
        {
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
    }

//    void ComputeVelocity()
//   {
//       velocity.x = Mathf.Cos(Mathf.Deg2Rad*angle);
//       velocity.y = Mathf.Sin(Mathf.Deg2Rad*angle);
//    }
}
