﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vector{
    public float x;
    public float y;
    public float angle;

    public Vector(float x, float y)
    {
        this.x = x * Time.deltaTime;
        this.y = y * Time.deltaTime;
    }

    public float GetAngle()
    {
        var angle = Mathf.Atan2(y, x) * Mathf.Rad2Deg;
        return angle;
    }
}
